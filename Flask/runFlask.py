from flask import Flask, request, render_template 
import runPygbag
import sqlite3


# Start PyGame
runPygbag.runProcessPacman('.\\Games\\pacman-python-master\\pacman') # Pacman process must be running for other Pygames to work, for some reason


# Creates the Flask application 
app = Flask(__name__) 


# Establishing variables for user sign-in
current_username = ""
current_password = ""
current_user_id	= ""
signedIn = False
count = 0


# Establish database connection
connect = sqlite3.connect(r".\Database\User_Info.db", check_same_thread=False) 
connect.execute('CREATE TABLE IF NOT EXISTS User_Info (User_Id INTEGER PRIMARY KEY AUTOINCREMENT, Username TEXT, Password TEXT)')
cursor = connect.cursor()


# Redirect to home page. Shows TV static
@app.route("/") 
def home(): 
	return render_template('redirectToHomePage.html')


# http://127.0.0.1:5000/home-page (Establishing our home page)
@app.route("/home_page") 
def home_page(): 
	global count
	count = 0
	return render_template('homePage.html', message=current_username, message2 = current_password)


# http://127.0.0.1:5000/snake (runs our snake game)
@app.route("/snake") 
def serve_snake(): 
	runPygbag.runProcessSnake('.\\Games\\snake') # Begins the Pygbag process in CMD for Snake on port 8001
	message = "Redirecting to Snake in Pygbag!" 
	return render_template('redirectToSnake.html', message=message) # Loads the redirect html to load the URL http://localhost:8001/


# http://127.0.0.1:5000/pacman (runs pacman game)
@app.route("/pacman") 
def serve_pacman(): 
	runPygbag.runProcessPacman('.\\Games\\pacman-python-master\\pacman') # Begins the Pygbag process in CMD for Pacman on port 8000
	message = "Redirecting to Pacman in Pygbag!" 
	return render_template('redirectToPacman.html', message=message) # Loads the redirect html to load the URL http://localhost:8000/


# http://127.0.0.1:5000/td (runs TD game)
@app.route('/td')
def serve_td():
	runPygbag.runProcessTD('.\\Games\\Tower-Defence-Game') # Begins the Pygbag process in CMD for TD on port 8002
	message = "Redirecting to TD in Pygbag!" 
	return render_template('redirectToTD.html', message=message) # Loads the redirect html to load the URL http://localhost:8002/


# Sign-in page to interact with our SQLite database via a GET request
@app.route('/sign-in', methods=['GET','POST'])
def sign_in():
	global signedIn
	signedIn = False
	global count

	# Sign in request method
	if request.method == 'GET': 

		# Retrieve the information that the user submitted
		Password = request.args.get('password')
		Username = request.args.get('first')

		# Fetch and assign user_info database to a variable
		cursor.execute("select * from User_Info")
		user_info = cursor.fetchall()

		# Change to remove case sensitivity for username
		if Username:
			Username = Username.lower()

		# Iterate through each row of user_info database (username/password combinations)
		for row in user_info:

			# If correct info
			if Username == row[1] and Password == row[2]:
				signedIn = True # User is signed in

				# Establish appropriate current user info
				global current_user_id
				global current_username
				global current_password
				current_password = Password
				current_username = Username
				current_user_id	= row[0]

				# Return to home page
				count = 0
				return render_template('redirectToHomePage.html')
		
		# If the user enters invalid information, alert and prompt a retry
		if signedIn == False:
			if count > 0:
				return render_template('signInPage.html', message="Incorrect username or password. Please try again!")
			count +=1

	return render_template('signInPage.html', message='Enter your username and password')

 
 
# 'Create account' page to interact with SQLite via a POST request
@app.route('/create-account', methods=['POST','GET'])
def create_account(): 

	# Detecting for the POST request 
	if request.method == 'POST': 

		# Gather information from the HTML form
		Username = request.form['first']
		Password = request.form['password']

		# Connect to the SQLite DB
		with sqlite3.connect(r".\Database\User_Info.db", check_same_thread=False) as users: 
			cursor = users.cursor() 

			# Insert the information into the SQLite database
			cursor.execute("INSERT INTO User_Info (Username, Password) VALUES (?,?)", (Username.lower(), Password)) 
			users.commit() 

			# Redirect to home page
			return render_template("redirectToHomePage.html")
	return render_template("createAccountPage.html")


# Allows user to modify their account info as it is stored in the database
@app.route('/modify-account', methods=['POST','GET'])
def modify_account():

	# bringing in the global vars
	global signedIn
	global current_password
	global current_username
	global current_user_id

	# If the user isn't signed in
	if signedIn == False:
		return render_template('redirectToSignIn.html', message='Please sign in to modify your account.')

	# Detecting for the submission of data
	if request.method == 'POST':

		# Establish DB connection
		with sqlite3.connect(r".\Database\User_Info.db", check_same_thread=False) as users: 

			# Taking in new user info
			new_username = request.form['first']
			new_password = request.form['password']

			# Running the SQL command
			cursor = users.cursor() 
			cursor.execute("UPDATE User_Info SET Username = (?), Password = (?) WHERE User_Id = (?)", (new_username.lower(), new_password, current_user_id)) 

			users.commit()
			return render_template('redirectToHomePage.html')
	
	return render_template('modifyAccount.html')


# Confirmation page to confirm account deletion
@app.route('/confirm-deletion')
def confirm_deletion():
	return render_template('confirmDeletion.html')


# Delete account
@app.route('/delete-account')
def delete_account():

	# Establish global var for User_Id and signed_in
	global current_user_id
	global signedIn

	# If the user is signed in
	if signedIn == True:
		# Establish a DB connection
		with sqlite3.connect(r".\Database\User_Info.db", check_same_thread=False) as users: 
			
			# Run DB commands
			cursor = users.cursor() 
			cursor.execute(f"DELETE FROM User_Info WHERE User_Id = (?)", (current_user_id,)) 
			users.commit()

			# Sign User Out
			signedIn = False

	# If the user isn't signed in
	elif signedIn == False:
		return render_template('redirectToSignIn.html', message='Please sign in to modify your account.')
	return render_template('deleteAccount.html') 

@app.route('/mj')
def mj():
	return render_template('michaelJackson.html')

# Runs the Flask application 
if __name__ == "__main__": 
	app.run(debug=True)