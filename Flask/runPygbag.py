import subprocess

# Function to run Pacman
def runProcessPacman(directory:str):
    subprocess.Popen(f'C:\\pygbag.exe --port 8000 --ume_block=0 {directory}')

# Function to run Snake
def runProcessSnake(directory:str):
    subprocess.Popen(f'C:\\pygbag.exe --port 8001 --ume_block=0 {directory}')

# Function to run TD
def runProcessTD(directory:str):
    subprocess.Popen(f'C:\\pygbag.exe --port 8002 --ume_block=0 {directory}')