import pygame
import random

# pygame setup
pygame.init()
clock = pygame.time.Clock()
FPS = 60
running = True
dt = 0

# game window
TITLE = 'Sovereign Saga'
SCREEN_WIDTH = 1920
SCREEN_HEIGHT = 1080
BOTTOM_PANEL = SCREEN_HEIGHT / 3
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption(TITLE)


# load and play music
background_music = r'Assets\background_music.mp3'
pygame.mixer.init()
pygame.mixer.music.load(background_music)
pygame.mixer.music.play(loops=-1)

# constant variables
POSITIONAL_OFFSET = 40
LARGE_OFFSET = 70
NAME = 'Player'

# game variables 
current_fighter = 1
total_fighters = 3
action_cooldown = 0
action_wait = 90
attack = False
potion = False
clicked = False
game_over = 0

# region load images
# main menu background image
main_menu_bg_img = pygame.image.load(r'Assets\main_menu_bg.jpg').convert_alpha()
# game background image
game_background_img = pygame.image.load(r'Assets\Cropped_Background.jpg').convert_alpha()
# panel image
panel_img = pygame.image.load(r'Assets\panel.png').convert_alpha()
# sword image
sword_img = pygame.image.load(r'Assets\sword.png').convert_alpha()
# potion image
potion_img = pygame.image.load(r'Assets\potion.png').convert_alpha()
# victory image
victory_img = pygame.image.load(r'Assets\victory.png').convert_alpha()
# defeat image
defeat_img = pygame.image.load(r'Assets\defeat.png').convert_alpha()
# next image
next_img = pygame.image.load(r'Assets\next.png').convert_alpha()
# play image
# play_img = pygame.image.load(r'Assets\play.png').convert_alpha()
# endregion

# define fonts
title_font = pygame.font.SysFont('Javanese Text', 100)
font = pygame.font.SysFont('Times New Roman', 48)

# define colors
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

# draw text
def draw_text(text: str, font, text_color, x, y):
    img = font.render(text, True, text_color)
    screen.blit(img, (x, y))

# draw background
def draw_bg(img):
    screen.blit(img, (0, 0))

# draw panel for displaying information
def draw_panel():
    # draw rectangle for panel ui
    screen.blit(panel_img, (0, SCREEN_HEIGHT - BOTTOM_PANEL))

    # show player stats
    draw_text(f'{NAME} HP: {player.hp}', font, BLACK, 100, SCREEN_HEIGHT - BOTTOM_PANEL + POSITIONAL_OFFSET)

    # show enemy stats
    for count, i in enumerate(bandit_list):
        draw_text(f'Bandit HP: {i.hp}', font, RED, 1000, (SCREEN_HEIGHT - BOTTOM_PANEL + POSITIONAL_OFFSET) + count * LARGE_OFFSET)
    
class Character():
    CHAR_SCALE_FACTOR = 5
    def __init__(self, x, y, name, max_hp, strength, potions):
        self.name = name
        self.max_hp = max_hp
        self.hp = max_hp
        self.strength = strength
        self.init_potions = potions
        self.potions = potions
        self.alive = True
        self.animation_list = []
        self.frame_index = 0
        # Action - 0: Idle, 1: Attack, 2: Hurt, 3: Dead
        self.action = 0
        self.update_time = pygame.time.get_ticks()

        # load idle images
        temp_list = []
        for i in range(8):
            img = pygame.image.load(f'Assets\{self.name}\Idle\{i}.png').convert_alpha()
            img = pygame.transform.scale(img, (img.get_width() * Character.CHAR_SCALE_FACTOR, img.get_height() * Character.CHAR_SCALE_FACTOR))
            temp_list.append(img)
        self.animation_list.append(temp_list)

        # load attack images
        temp_list = []
        for i in range(8):
            img = pygame.image.load(f'Assets\{self.name}\Attack\{i}.png').convert_alpha()
            img = pygame.transform.scale(img, (img.get_width() * Character.CHAR_SCALE_FACTOR, img.get_height() * Character.CHAR_SCALE_FACTOR))
            temp_list.append(img)
        self.animation_list.append(temp_list)
        
        # load hurt images
        temp_list = []
        for i in range(3):
            img = pygame.image.load(f'Assets\{self.name}\Hurt\{i}.png').convert_alpha()
            img = pygame.transform.scale(img, (img.get_width() * Character.CHAR_SCALE_FACTOR, img.get_height() * Character.CHAR_SCALE_FACTOR))
            temp_list.append(img)
        self.animation_list.append(temp_list)

        self.image = self.animation_list[self.action][self.frame_index]
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)

        # load death images
        temp_list = []
        for i in range(10):
            img = pygame.image.load(f'Assets\{self.name}\Death\{i}.png').convert_alpha()
            img = pygame.transform.scale(img, (img.get_width() * Character.CHAR_SCALE_FACTOR, img.get_height() * Character.CHAR_SCALE_FACTOR))
            temp_list.append(img)
        self.animation_list.append(temp_list)

        self.image = self.animation_list[self.action][self.frame_index]
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)


    def update(self):
        animation_cooldown = 100
        self.image = self.animation_list[self.action][self.frame_index]
        if pygame.time.get_ticks() - self.update_time > animation_cooldown:
            self.update_time = pygame.time.get_ticks()
            self.frame_index += 1

        if self.frame_index >= len(self.animation_list[self.action]):
            if self.alive == False:
                self.frame_index = len(self.animation_list[self.action]) - 1
            else:
                self.idle()

    def idle(self):
        self.action = 0
        self.frame_index = 0
        self.update_time = pygame.time.get_ticks()

    def hurt(self):
        self.action = 2
        self.frame_index = 0
        self.update_time = pygame.time.get_ticks()

    def death(self):
        self.action = 3
        self.frame_index = 0
        self.update_time = pygame.time.get_ticks()

    def attack(self, target):
        # deal damage to target
        random_num = random.randint(-5, 5)
        damage = self.strength + random_num
        target.hp -= damage
        target.hurt()
        # check if target died
        if target.hp < 1:
            target.hp = 0
            target.alive = False
            target.death()
        # set animation to attack
        self.action = 1
        self.frame_index = 0
        self.update_time = pygame.time.get_ticks()

    def reset(self):
        self.alive = True
        self.potions = self.init_potions
        self.hp = self.max_hp
        self.frame_index = 0
        self.action = 0
        self.update_time = pygame.time.get_ticks()

    def draw(self):
        screen.blit(self.image, self.rect)

# initialize characters
player = Character(300, 580, 'Knight', 30, 10, 3)
bandit_1 = Character(1320, 590, 'Bandit', 20, 5, 0)
bandit_2 = Character(1620, 590, 'Bandit', 15, 5, 1)

bandit_list = []
bandit_list.append(bandit_1)
bandit_list.append(bandit_2)

class HealthBar():
    def __init__(self, x, y, hp, max_hp) -> None:
        self.x = x
        self.y = y
        self.hp = hp
        self.max_hp = max_hp

    def draw(self, hp):
        self.hp = hp
        # calculate health ratio
        ratio = self.hp / self.max_hp
        pygame.draw.rect(screen, RED, (self.x, self.y, 500, 20))
        pygame.draw.rect(screen, GREEN, (self.x, self.y, 500 * ratio, 20))

# initialize healthbars
player_health = HealthBar(100, SCREEN_HEIGHT - BOTTOM_PANEL + 90, player.hp, player.max_hp)
bandit_1_health = HealthBar(1000, SCREEN_HEIGHT - BOTTOM_PANEL + 90, bandit_1.hp, bandit_1.max_hp)
bandit_2_health = HealthBar(1000, SCREEN_HEIGHT - BOTTOM_PANEL + 160, bandit_2.hp, bandit_2.max_hp)

class Button():
	def __init__(self, surface, x, y, image, size_x, size_y):
		self.image = pygame.transform.scale(image, (size_x, size_y))
		self.rect = self.image.get_rect()
		self.rect.topleft = (x, y)
		self.clicked = False
		self.surface = surface

	def draw(self):
		action = False

		#get mouse position
		pos = pygame.mouse.get_pos()

		#check mouseover and clicked conditions
		if self.rect.collidepoint(pos):
			if pygame.mouse.get_pressed()[0] == 1 and self.clicked == False:
				action = True
				self.clicked = True

		if pygame.mouse.get_pressed()[0] == 0:
			self.clicked = False

		#draw button
		self.surface.blit(self.image, (self.rect.x, self.rect.y))

		return action

# create buttons
potion_button = Button(screen, player_health.x, player_health.y + 50, potion_img, 64, 64)
next_button = Button(screen, (SCREEN_WIDTH / 2.5) + LARGE_OFFSET + POSITIONAL_OFFSET, (SCREEN_HEIGHT / 4) + LARGE_OFFSET, next_img, 130, 50)
# play_button = Button(screen, SCREEN_WIDTH / 3, SCREEN_HEIGHT / 7, play_img, 260, 125)


# def main_menu():
#     pygame.display.set_caption("Main Menu")

#     while running:
#         draw_bg(main_menu_bg_img)

#         pos = pygame.mouse.get_pos()

#         draw_text('Sovreign Saga', title_font, GREEN, SCREEN_WIDTH / 3, 40)

#         if play_button.draw():
#             play()
        

while running:
    # draw background
    draw_bg(game_background_img)

    # draw panel
    draw_panel()
    player_health.draw(player.hp)
    bandit_1_health.draw(bandit_1.hp)
    bandit_2_health.draw(bandit_2.hp)

    # draw characters
    player.update()
    player.draw()

    for bandit in bandit_list:
        bandit.update()
        bandit.draw()
    
    # control player actions
    # reset action variables
    attack = False
    potion = False
    target = None

    pygame.mouse.set_visible(True)
    pos = pygame.mouse.get_pos()
    for count, bandit in enumerate(bandit_list):
        if bandit.rect.collidepoint(pos):
            pygame.mouse.set_visible(False)
            screen.blit(sword_img, pos)

            if clicked == True and bandit.alive == True:
                attack = True
                target = bandit_list[count]

    if potion_button.draw():
        potion = True
    draw_text(str(player.potions), font, RED, 70, player_health.y + 50)

    if game_over == 0:
        # player action
        if player.alive == True:
            if current_fighter == 1:
                action_cooldown += 1
                if action_cooldown >= action_wait:
                    # look for player action
                    # attack
                    if attack == True and target != None:
                        player.attack(target)
                        current_fighter += 1
                        action_cooldown = 0
                    # potion
                    if potion == True:
                        if player.potions > 0:
                            # check if potion heals beyond max health
                            if player.max_hp - player.hp > 10:
                                player.hp += 10
                            else:
                                player.hp = player.max_hp
                            player.potions -= 1
                            current_fighter += 1
                            action_cooldown = 0
        else:
            game_over = -1


        # enemy action
        for count, bandit in enumerate(bandit_list):
            if current_fighter == 2 + count:
                if bandit.alive == True:
                    action_cooldown += 1
                    if action_cooldown >= action_wait:
                        # check if enemy needs to heal
                        if (bandit.hp / bandit.max_hp) < 0.5 and bandit.potions > 0:
                            # check if potion heals beyond max health
                            if bandit.max_hp - bandit.hp > 5:
                                bandit.hp += 5
                            else:
                                bandit.hp = bandit.max_hp
                            bandit.potions -= 1
                            current_fighter += 1
                            action_cooldown = 0
                        else:
                            # attack
                            bandit.attack(player)
                            current_fighter += 1
                            action_cooldown = 0
                else:
                    current_fighter += 1
                
        # reset round
        if current_fighter > total_fighters:
            current_fighter = 1

    # check if enemies are dead
    alive = 0
    for bandit in bandit_list:
        if bandit.alive == True:
            alive += 1
    if alive == 0:
        game_over = 1

    # check if game is over
    if game_over != 0:
        if game_over == 1:
            screen.blit(victory_img, ((SCREEN_WIDTH / 2.5) + POSITIONAL_OFFSET, SCREEN_HEIGHT / 4))
        if game_over == -1:
            screen.blit(defeat_img, ((SCREEN_WIDTH / 2.5) + POSITIONAL_OFFSET + 20, SCREEN_HEIGHT / 4))
        
        if next_button.draw():
            player.reset()
            for bandit in bandit_list:
                bandit.reset()
            current_fighter = 1
            action_cooldown = 0
            game_over = 0

    # event handler
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            clicked = True
        else:
            clicked = False
    
    pygame.display.update()

    # limits FPS to 60
    # dt is delta time in seconds since last frame, used for framerate-
    # independent physics.
    dt = clock.tick(FPS) / 1000

pygame.quit()