ENEMY_SPAWN_DATA = [
  {
    #1
    "weak": 15,
    "medium": 0,
    "strong": 0,
    "elite": 0
  },
  {
    #2
    "weak": 20,
    "medium": 5,
    "strong": 0,
    "elite": 0
  },
  {
    #3
    "weak": 30,
    "medium": 5,
    "strong": 0,
    "elite": 0
  },
  {
    #4
    "weak": 40,
    "medium": 10,
    "strong": 0,
    "elite": 0
  },
  {
    #5
    "weak": 40,
    "medium": 15,
    "strong": 0,
    "elite": 0
  },
  {
    #6
    "weak": 30,
    "medium": 10,
    "strong": 4,
    "elite": 0
  },
  {
    #7
    "weak": 40,
    "medium": 25,
    "strong": 8,
    "elite": 0
  },
  {
    #8
    "weak": 20,
    "medium": 20,
    "strong": 15,
    "elite": 0
  },
  {
    #9
    "weak": 30,
    "medium": 20,
    "strong": 10,
    "elite": 0
  },
  {
    #10
    "weak": 0,
    "medium": 100,
    "strong": 0,
    "elite": 0
  },
  {
    #11
    "weak": 10,
    "medium": 20,
    "strong": 24,
    "elite": 4
  },
  {
    #12
    "weak": 0,
    "medium": 30,
    "strong": 20,
    "elite": 10
  },
  {
    #13
    "weak": 40,
    "medium": 0,
    "strong": 50,
    "elite": 20
  },
  {
    #14
    "weak": 30,
    "medium": 30,
    "strong": 30,
    "elite": 30
  },
  {
    #15
    "weak": 50,
    "medium": 50,
    "strong": 50,
    "elite": 50
  }
]

ENEMY_DATA = {
    "weak": {
    "health": 1,
    "speed": 1
  },
    "medium": {
    "health": 3,
    "speed": 3
  },
    "strong": {
    "health": 6,
    "speed": 4
  },
    "elite": {
    "health": 12,
    "speed": 6
  }
}