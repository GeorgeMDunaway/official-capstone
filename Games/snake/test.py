import asyncio
import pygame

async def main():
    while True:
        running = True
        await asyncio.sleep(0)
        if not running:
            pygame.quit()
            return

asyncio.run(main())